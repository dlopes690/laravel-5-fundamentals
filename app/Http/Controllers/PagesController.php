<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PagesController extends Controller
{
    public function index()
    {
    	return view('welcome');
    }

    public function about()
    {
    	// $name = [];
    	// $name['first'] = 'Danny';
    	// $name['last'] = 'Lopes';
    	$name = 'Danny';

    	
    	// return view('pages.about', $name);
    	return view('pages.about', compact('name'));
    }

    public function contact()
    {
    	return view('pages.contact');
    }
}
